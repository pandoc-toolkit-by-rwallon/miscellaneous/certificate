# Changelog

This file describes the evolution of the *Certificate* LaTeX template for
Pandoc.

Note that versions are numbered using the `BREAKING.FEATURE.FIX` scheme.

## Version 0.1.0 (October 2019)

+ Frame style can be set from YAML.
+ Colors can be set from YAML.
+ Content can be customized from YAML.
