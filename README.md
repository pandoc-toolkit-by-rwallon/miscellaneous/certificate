# Pandoc Template for *Certificates*

[![pipeline status](https://gitlab.com/pandoc-toolkit-by-rwallon/miscellaneous/certificate/badges/master/pipeline.svg)](https://gitlab.com/pandoc-toolkit-by-rwallon/miscellaneous/certificate/commits/master)

## Description

This project provides a template allowing an easy creation of certificates
with [Pandoc](https://pandoc.org), inspired by
[Fran's answer on Stack Exchange](https://tex.stackexchange.com/questions/46406/package-for-certificates#answer-73680).

This template is distributed under a Creative Commons Attribution 4.0
International License.

[![CC-BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)

## Requirements

To build your certificate using this template, [Pandoc](https://pandoc.org)
at least 2.0 have to be installed on your computer.

You also need to have the packages `niceframe` and `ulem` installed in your
[LaTeX](https://www.latex-project.org) distribution.

## Creating your Certificate

To customize the certificate to your own use case, you need to setup the YAML
configuration.
For convenience, we provide a [`metadata.md`](./metadata.md) file in which all
possible settings are specified, so that you can just set them or remove the
ones which do not fit your needs.
Read below for details on how to configure your certificate, and see our
various [examples](examples).

### Language Configuration

Specify the language to use by `babel` by setting `language` accordingly.
The default language is `english`.

Also, if you want to replace the *"to"* in the certificate by a custom word or
phrase, set `to` appropriately.

### Frame Style

You may set the value of `frame` to one of `square`, `simplesquare`,
`roundedsquare`, `weavedsquare`, `simpleround` or `weavedround` to configure
the frame style to be used.

The default frame style is `square`.

### Colors

#### Color Definitions

You may define your own colors through the variable `color`, which must be
set to a list of objects defining three fields:

+ `name`: the name of the color you are defining.
+ `type`: the type of the color, among `gray`, `rgb`, `RGB`, `html`, `HTML`,
  etc.
+ `value`: the actual value of the color.

Note that each color is then automatically translated into LaTeX by using the
following command:

```latex
\definecolor{name}{type}{value}
```

#### Color Settings

Once you have defined your own colors, you may set the following properties
to customize the colors of your certificate (of course, you may also use
predefined colors):

+ `frame-color`: the color for the frame of the certificate.
+ `curly-color`: the color for the curly signs.
+ `institute-color`: the color for the name of the institute.
+ `introduction-color`: the color for the introduction.
+ `name-color`: the color for the name of the certificate itself.
+ `title-color`: the color for the title of the certificate.
+ `receiver-color`: the color for the name of the person who receives the
  certificate.
+ `jury-color`: the color for the jury.

By default, all these colors are set to `black`.

### Certifying Institute

You may set the logo of the institute delivering the certificate by setting
`logo` to its path.

Use `institute` to set the name of the institute.

### Informations about the Certificate

You may set through `introduction` a small sentence describing the certificate.

Moreover, you may specify within `degree` the `name` of the certificate
(e.g. *certificate*, *diploma*, *degree*, ...), its `title` and its `level`.

### Person being Certified

The `receiver` is the name of the person to whom the certificate is delivered.

If the receiver is a member of a team, you may specify its name in `team`.

### Jury of the Certificate

You may set into `jury` the list of the members of the jury delivering the
certificate, by giving their `name` and `position`.

## Building your Certificate

Suppose that you have written the metadata of the certificate in a file
`input.md`.
Then, to produce the certificate in a file named `output.pdf`, execute the
following command:

```bash
pandoc --template certificate.pandoc -o output.pdf input.md
```
