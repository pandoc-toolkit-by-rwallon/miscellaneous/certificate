# Use Cases of the *Certificate* Template

This directory contains examples illustrating different use cases of the Pandoc
template for producing certificates.

In particular, each of the examples, in addition to using a different set of
colors, also uses a different frame style, namely:

+ `square`:
  see the metadata [here](square-frame.md) and the output PDF
  [here](/../builds/artifacts/master/file/examples/square-frame.pdf?job=make-examples).

+ `simplesquare`:
  see the metadata [here](simplesquare-frame.md) and the output PDF
  [here](/../builds/artifacts/master/file/examples/simplesquare-frame.pdf?job=make-examples).

+ `roundedsquare`:
  see the metadata [here](roundedsquare-frame.md) and the output PDF
  [here](/../builds/artifacts/master/file/examples/roundedsquare-frame.pdf?job=make-examples).

+ `weavedsquare`:
  see the metadata [here](weavedsquare-frame.md) and the output PDF
  [here](/../builds/artifacts/master/file/examples/weavedsquare-frame.pdf?job=make-examples).

+ `simpleround`:
  see the metadata [here](simpleround-frame.md) and the output PDF
  [here](/../builds/artifacts/master/file/examples/simpleround-frame.pdf?job=make-examples).

+ `weavedround`:
  see the metadata [here](weavedround-frame.md) and the output PDF
  [here](/../builds/artifacts/master/file/examples/weavedround-frame.pdf?job=make-examples).
