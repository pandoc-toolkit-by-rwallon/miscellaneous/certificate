---
# Frame Style.
frame: simplesquare

# Color Definitions.
color:
    - name: darkliver
      type: HTML
      value: 50514F
    - name: mustard
      type: HTML
      value: FFE066
    - name: lapislazuli
      type: HTML
      value: 247BA0
    - name: tangopink
      type: HTML
      value: DB6C79
    - name: greensheen
      type: HTML
      value: 70C1B3

# Color Settings.
frame-color: mustard
curly-color: tangopink
institute-color: lapislazuli
introduction-color: darkliver
name-color: greensheen
title-color: greensheen
receiver-color: darkliver
jury-color: lapislazuli

# Certifying Institute.
logo: latex.png
institute: University of Pandoc

# Informations about the Certificate.
introduction: |
    In honour of outstanding performance and dedication to waste time in
    class we hereby award the
degree:
    name: Certificate of
    title: Biggest Sleeper Class
    level: Master Degree

# Person being Certified.
receiver: Mr. Dormouse Overwintering Marmot
team: The Sleepers

# Jury of the Certificate.
jury:
    - name: Dr. David Restless
      position: Head of Department
    - name: Dr. Peter Awakened
      position: Examinor
    - name: Dr. John Workerhard
      position: Academic Advisor
---
