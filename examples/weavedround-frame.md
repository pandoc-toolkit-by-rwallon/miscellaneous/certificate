---
# Frame Style.
frame: weavedround

# Color Definitions.
color:
    - name: tuscan
      type: HTML
      value: FBD1A2
    - name: pearlaqua
      type: HTML
      value: 7DCFB6
    - name: turquoisesurf
      type: HTML
      value: 00B2CA
    - name: tangopink
      type: HTML
      value: DB6C79
    - name: yaleblue
      type: HTML
      value: 1D4E89

# Color Settings.
frame-color: turquoisesurf
curly-color: pearlaqua
institute-color: pearlaqua
introduction-color: yaleblue
name-color: tangopink
title-color: tuscan
receiver-color: yaleblue
jury-color: turquoisesurf

# Certifying Institute.
logo: latex.png
institute: University of Pandoc

# Informations about the Certificate.
introduction: |
    In honour of outstanding performance and dedication to waste time in
    class we hereby award the
degree:
    name: Certificate of
    title: Biggest Sleeper Class
    level: Master Degree

# Person being Certified.
receiver: Mr. Dormouse Overwintering Marmot
team: The Sleepers

# Jury of the Certificate.
jury:
    - name: Dr. David Restless
      position: Head of Department
    - name: Dr. Peter Awakened
      position: Examinor
    - name: Dr. John Workerhard
      position: Academic Advisor
---
