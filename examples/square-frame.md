---
# Frame Style.
frame: square

# Color Settings.
frame-color: green!30!black!60
curly-color: green!30!black!60
institute-color: red!10!black!90
introduction-color: green!10!black!90
name-color: red!30!black!90
receiver-color: green!30!black!60
jury-color: blue!40!black

# Certifying Institute.
logo: latex.png
institute: University of Pandoc

# Informations about the Certificate.
introduction: |
    In honour of outstanding performance and dedication to waste time in
    class we hereby award the
degree:
    name: Certificate of
    title: Biggest Sleeper Class
    level: Master Degree

# Person being Certified.
receiver: Mr. Dormouse Overwintering Marmot
team: The Sleepers

# Jury of the Certificate.
jury:
    - name: Dr. David Restless
      position: Head of Department
    - name: Dr. Peter Awakened
      position: Examinor
    - name: Dr. John Workerhard
      position: Academic Advisor
---
