---
# Frame Style.
frame: weavedsquare

# Color Definitions.
color:
    - name: yelloworange
      type: HTML
      value: FFBC42
    - name: ruby
      type: HTML
      value: D81159
    - name: darkraspberry
      type: HTML
      value: 8F2D56
    - name: celadongreen
      type: HTML
      value: 218380
    - name: middleblue
      type: HTML
      value: 73D2DE

# Color Settings.
frame-color: yelloworange
curly-color: ruby
institute-color: middleblue
introduction-color: celadongreen
name-color: darkraspberry
title-color: darkraspberry
receiver-color: celadongreen
jury-color: middleblue

# Certifying Institute.
logo: latex.png
institute: University of Pandoc

# Informations about the Certificate.
introduction: |
    In honour of outstanding performance and dedication to waste time in
    class we hereby award the
degree:
    name: Certificate of
    title: Biggest Sleeper Class
    level: Master Degree

# Person being Certified.
receiver: Mr. Dormouse Overwintering Marmot
team: The Sleepers

# Jury of the Certificate.
jury:
    - name: Dr. David Restless
      position: Head of Department
    - name: Dr. Peter Awakened
      position: Examinor
    - name: Dr. John Workerhard
      position: Academic Advisor
---
