---
# Frame Style.
frame: simpleround

# Color Definitions.
color:
    - name: pastelgreen
      type: HTML
      value: 7DDF64
    - name: mediumspringbud
      type: HTML
      value: C0DF85
    - name: burlywood
      type: HTML
      value: DEB986
    - name: tangopink
      type: HTML
      value: DB6C79
    - name: infrared
      type: HTML
      value: ED4D6E

# Color Settings.
frame-color: infrared
curly-color: pastelgreen
name-color: burlywood
title-color: tangopink
jury-color: mediumspringbud

# Certifying Institute.
logo: latex.png
institute: University of Pandoc

# Informations about the Certificate.
introduction: |
    In honour of outstanding performance and dedication to waste time in
    class we hereby award the
degree:
    name: Certificate of
    title: Biggest Sleeper Class
    level: Master Degree

# Person being Certified.
receiver: Mr. Dormouse Overwintering Marmot
team: The Sleepers

# Jury of the Certificate.
jury:
    - name: Dr. David Restless
      position: Head of Department
    - name: Dr. Peter Awakened
      position: Examinor
    - name: Dr. John Workerhard
      position: Academic Advisor
---
