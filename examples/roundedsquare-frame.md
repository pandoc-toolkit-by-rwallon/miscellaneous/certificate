---
# Frame Style.
frame: roundedsquare

# Certifying Institute.
logo: latex.png
institute: University of Pandoc

# Informations about the Certificate.
introduction: |
    In honour of outstanding performance and dedication to waste time in
    class we hereby award the
degree:
    name: Certificate of
    title: Biggest Sleeper Class
    level: Master Degree

# Person being Certified.
receiver: Mr. Dormouse Overwintering Marmot
team: The Sleepers

# Jury of the Certificate.
jury:
    - name: Dr. David Restless
      position: Head of Department
    - name: Dr. Peter Awakened
      position: Examinor
    - name: Dr. John Workerhard
      position: Academic Advisor
---
