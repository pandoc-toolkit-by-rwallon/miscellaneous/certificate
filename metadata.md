---
# Language Configuration.
language:
to:

# Frame Style.
frame:

# Color Definitions.
color:
    - name:
      type:
      value:

# Color Settings.
frame-color:
curly-color:
institute-color:
introduction-color:
name-color:
title-color:
reveiver-color:
jury-color:

# Certifying Institute.
logo:
institute:

# Informations about the Certificate.
introduction:
degree:
    name:
    title:
    level:

# Person being Certified.
receiver:

# Jury of the Certificate.
jury:
    - name:
      position:
---
